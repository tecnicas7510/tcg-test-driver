package ar.uba.fi.tdd.tcg.driver;

public enum DriverGameMode {
    HitpointLoss,
    CreatureSlayer
}
