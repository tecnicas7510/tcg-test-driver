package ar.uba.fi.tdd.tcg.driver;

public enum DriverTurnPhase {
    Initial,
    Main,
    Attack,
    End
}
