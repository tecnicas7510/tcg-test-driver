package ar.uba.fi.tdd.tcg.driver;

public enum DriverActiveZone {
    Combat,
    Reserve,
    Artifact
}
