package ar.uba.fi.tdd.tcg.driver;

public enum DriverEnergyType {
    Water,
    Fire,
    Plant
}
